all: p0953.time p1249.time

.PRECIOUS: p0953_list.cxx p1240_list.cxx

%_list.cxx: %head.cxx %one.cxx
	cat $*head.cxx > $@
	( for i in {0..10000}; do sed s,@,$$i,g $*one.cxx; done ) >> $@

%.time: %_list.cxx
	time ( clang++ -std=c++2a -fsyntax-only $< )

clean:
	rm -f *_list.cxx
