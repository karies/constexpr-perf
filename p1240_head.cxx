#include <array>

enum Dummy{a, b, c};

struct Info {};

constexpr std::array<Info,3> enumerators(Info) { return {}; }
constexpr int value(Info) { return 42; }
constexpr std::array<char, 128> name(Info) { return {}; }

constexpr Info reflexpr() { return {}; }
