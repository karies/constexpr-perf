template <class T>
constexpr std::array<char, 128> get_name_@(int v) {
   for (Enumerator e: reflexpr(/*T*/).enumerators()) {
      if (e.value() == v)
         return e.name();
   }
   return {};
}

constexpr auto eval@ = get_name_@<Dummy>(1);
