#include <array>

enum Dummy{a, b, c};

struct Enumerator {
   constexpr std::array<char, 128> name() { return {}; }
   constexpr int value() { return 42; }
};
struct Enumeration {
   constexpr std::array<Enumerator, 3> enumerators() { return {}; }
};

constexpr Enumeration reflexpr() { return {}; }
