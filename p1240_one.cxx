template <class T>
constexpr std::array<char, 128> get_name_@(int v) {
   for (Info e: enumerators(reflexpr(/*T*/))) {
      if (value(e) == v)
      return name(e);
   }
   return {};
}

constexpr auto eval@ = get_name_@<Dummy>(1);
